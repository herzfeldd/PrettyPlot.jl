module PrettyPlot

using PyPlot
using PyCall
using PyPlot
using EzXML
using LinearAlgebra
using Base64
using Statistics

# TODO Fix for shaded errorbar based on pycall problem. When fixed in PyCall, can
# be removed.
using PyCall: PyObject
#PyObject(x::Adjoint) = PyObject(copy(x))
PyObject(x::Transpose) = PyObject(copy(x))

# See https://github.com/JuliaPy/PyCall.jl#using-pycall-from-julia-modules
const mpl_patch = PyNULL()
const mpl = PyNULL()
const mpl_colors = PyNULL()
const mpl_collections = PyNULL()
const mpl_lines = PyNULL()
const mpl_text = PyNULL()
const mpl_containers = PyNULL()
const mpl_projections = PyNULL()
const mpl_toolkits = PyNULL()
const seaborn = PyNULL()

function __init__()
    copy!(mpl_patch, pyimport_conda("matplotlib.patches", "matplotlib"))
    copy!(mpl, pyimport_conda("matplotlib", "matplotlib"))
    copy!(mpl_colors, pyimport_conda("matplotlib.colors", "matplotlib"))
    copy!(mpl_collections, pyimport_conda("matplotlib.collections", "matplotlib"))
    copy!(mpl_lines, pyimport_conda("matplotlib.lines", "matplotlib"))
    copy!(mpl_text, pyimport_conda("matplotlib.text", "matplotlib"))
    copy!(mpl_containers, pyimport_conda("matplotlib.container", "matplotlib"))
    copy!(mpl_projections, pyimport_conda("matplotlib.projections", "matplotlib"))
    copy!(mpl_toolkits, pyimport_conda("mpl_toolkits", "mpl_toolkits")) #3d libraries
    copy!(seaborn, pyimport_conda("seaborn", "seaborn"))

        # Embed fonts in PDFs and SVGs
    # and change the default font to arial
    # See the documentation for PyPlot/PyCall for why
    # we need to cast this as a PyDict
    rcParams = PyPlot.PyDict(mpl."rcParams")
    rcParams["svg.fonttype"] = "none"
    rcParams["pdf.fonttype"] = 42
    rcParams["ps.fonttype"] = 42
    rcParams["pdf.use14corefonts"] = true
    rcParams["ps.useafm"] = true
    #rcParams["font.family"] = "arial"
    rcParams["font.family"] = "sans-serif"
    rcParams["font.sans-serif"] = ["Arial", "Helvetica"]
    rcParams["axes.unicode_minus"] = false  # Fixes negative sign issue (shows up as '?')
end

include("plot.jl")

end # module
