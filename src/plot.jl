
export shaded_errorbar, ppretty, save_figure_composer, dump_figure_composer, colorline, swarmplot


function shaded_errorbar(x::AbstractVector{<:Real}, y::AbstractVector{<:Real}, errors::AbstractMatrix{<:Real}; alpha::Real=0.25, y_error::Bool=false, linewidth=2.0, kwargs...)
    if length(x) != length(y) 
        error("Invalid sizes")
    end
    if size(errors, 1) != 2
        errors = errors'
    end
    @assert size(errors, 1) == 2 && size(errors, 2) == length(x) "Invalid sizes"

    if all(isnan.(errors)) || all(isnan.(x) .| isnan.(y))
        l = plot(x, y; linewidth=linewidth, kwargs...)
        return l[1], nothing
    end

    # Remove any NaNs from x and y (to ensure we have a complete patch)
    select = (isnan.(x) .|| isnan.(y) .|| dropdims(any(isnan.(errors), dims=1), dims=1))
    x = x[.!select]
    y = y[.!select]
    err = errors[:, .!select]

    # Plot the lines
    l = plot(x, y; linewidth=linewidth, kwargs...)
    # Note that 'l' here is an array with 1 element

    # Plot the patch
    xy = zeros(2, length(x) * 2) # First row is X values

    if y_error
        xy[1, :] = vcat(errors[1, :], reverse(errors[2, :], dims=1))
        xy[2, :] = vcat(y, reverse(y, dims=1))
        p = mpl_patch.Polygon(xy', alpha=alpha, facecolor=l[1].get_color(),
        edgecolor="none")
    else
        xy[1, :] = vcat(x, reverse(x, dims=1))
        xy[2, :] = vcat(errors[1, :], reverse(errors[2, :], dims=1))
        p = mpl_patch.Polygon(Matrix(xy'), alpha=alpha, facecolor=l[1].get_color(),
        edgecolor="none")
    end
    ax = gca()
    ax.add_patch(p)

    # Save our patch for later
    if ! PyCall.hasproperty(ax, :shaded_errorbars)
        #ax[shaded_errorbars] = PyVector(PyAny[])
        #pybuiltin(:setattr)(ax, "shaded_errorbars", PyObject(Vector{Any}()))
        errorbars = Vector{Any}()
    else
        errorbars = Vector{Any}(ax.shaded_errorbars)
    end
    push!(errorbars, (l[1], p, errors, y_error))
    pybuiltin(:setattr)(ax, "shaded_errorbars", PyObject(errorbars))

    return l[1], p
end

function shaded_errorbar(x::AbstractVector{<:Real}, y::AbstractVector{<:Real}, errors::AbstractVector{<:Real}; y_error::Bool=false, kwargs...)
    @assert length(x) == length(y) && length(x) == length(errors)
    symmetric_error = zeros(2, length(x))
    if y_error == false
        symmetric_error[1, :] = y .+ errors
        symmetric_error[2, :] = y .- errors
    else
        symmetric_error[1, :] = x .+ errors
        symmetric_error[2, :] = x .- errors
    end

    vals = shaded_errorbar(x, y, symmetric_error; y_error=y_error, kwargs...)
    # Needed for figure composer
    ax = gca()
    if PyCall.hasproperty(ax, :shaded_errorbars)
        errorbars = Vector{Any}(ax.shaded_errorbars)
        temp = errorbars[end]
        errorbars[end] = (temp[1], temp[2], errors, temp[4],)
        pybuiltin(:setattr)(ax, "shaded_errorbars", PyObject(errorbars))
    end
    return vals
end

"""
    colorline(x, y, [cmap])

Plots a line that goes for one color to another based on the passed colormap.

Returns the set of LineCollections that were plotted.
"""
function colorline(x, y, cmap=nothing, norm=plt.Normalize(0, 1); kwargs...)
    z = collect(range(0, 1.0, length=length(x)))

    """
    Create a list of line segments from x and y coordinates, in the
    correct format for LineCollection: an array of the form numlines x (points per line)
    x 2 x (x and y) array

    That is, we have have x = [1, 2, 3] and y = [4, 5, 6], then the output should be
    a list of "segments" (1, 4) -> (2, 5) and (2, 5) -> (3, 6). Therefore, the output
    is [[1, 4], [2, 5]], [[2, 5], [3, 6]]
    """
    function make_segments(x, y)
        temp = reshape(vcat(x, y), length(x), 2)
        segments = reshape(vcat(temp[1:end-1, :], temp[2:end, :]), length(x) - 1, 2, 2)
        return segments
    end
    ax = gca()

    if cmap == nothing
        colormaps = ["copper", "gist_heat", "afmhot", "hot", "pink", "bone", "gray", "gist_gray"]
        cmap = colormaps[(length(ax.collections) % length(colormaps)) + 1]
    end

    segments = make_segments(x, y)
    l = plot(x, y, linewidth=0) # Plot so that we update our x and y limits
    l[1].remove()
    lc = mpl_collections.LineCollection(segments, array=z, cmap=cmap, norm=norm; kwargs...)
    ax.add_collection(lc)
    return lc
end

function ppretty(;square=false, fontsize=8)
    font = "sans-serif"
    
    fig = gcf()
    fig.set_facecolor("white") # Set the background to white
    tight_layout()

    if square
        fig.set_size_inches((2.0, 2.0), forward=true)
    else
        fig.set_size_inches((3.5, 2.0), forward=true)
    end

    for ax in fig.get_axes()
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        ax.set_facecolor("white")
        for o in ax.findobj(mpl_lines.Line2D)
            o.set_linewidth(1.5)
        end

        # Turn off the box
        if ax.xaxis.get_ticks_position() == "default"
            ax.xaxis.set_ticks_position("bottom")
            ax.xaxis.tick_bottom()
        end
        if ax.yaxis.get_ticks_position() == "default"
            ax.yaxis.set_ticks_position("left")
            ax.yaxis.tick_left()
        end

        if ax.yaxis.get_ticks_position() == "left"
            ax.spines["right"].set_visible(false)
            ax.spines["left"].set_linewidth(0.75)
        elseif ax.yaxis.get_ticks_position() == "right"
            ax.spines["left"].set_visible(false)
            ax.spines["right"].set_linewidth(0.75)
        else
            ax.spines["left"].set_linewidth(0.75)
            ax.spines["right"].set_linewidth(0.75)
        end

        if ax.xaxis.get_ticks_position() == "bottom"
            ax.spines["top"].set_visible(false)
            ax.spines["bottom"].set_linewidth(0.75)
        elseif ax.xaxis.get_ticks_position() == "top"
            ax.spines["bottom"].set_visible(false)
            ax.spines["top"].set_linewidth(0.75)
        else
            ax.spines["top"].set_linewidth(0.75)
            ax.spines["bottom"].set_linewidth(0.75)
        end

        ax.xaxis.labelpad = 2.5 # Move labels closer to axis
        ax.yaxis.labelpad = 2.5

        # Make ticks outside the plot
        ax.tick_params(direction="out", length=4.5, width=0.75)

        # Move the y-axis off to the left
        movement = 0.1 / fig.get_size_inches()[1]
        if ax.yaxis.get_ticks_position() == "left"
            ax.spines["left"].set_position(("axes", 0 - movement))
        end
        if ax.yaxis.get_ticks_position() == "right"
            ax.spines["right"].set_position(("axes", 1 + movement))
        end
        tick_params(axis="both", which="major", labelsize=8)

        if square
            ax.set_aspect(1.0/ax.get_data_ratio())
        end

        if ax.get_legend() !== nothing && length(ax.get_legend().get_texts()) > 0
            ax.legend(frameon=false, borderaxespad=0, borderpad=0, handletextpad=0.1, handleheight=0.1)
            ax.get_legend().draw_frame(false)
            texts = ax.get_legend().get_texts()
            for o in texts
                o.set_fontname(font)
                o.set_fontsize(fontsize)
            end
            for o in ax.get_legend().legendHandles
                o.set_linewidth(1.5)
            end
            for o in ax.get_legend().get_lines()
                # Make lines half their length
                x_data = o.get_xdata()
                x_data[1] = (x_data[2] - x_data[1]) / 2 + x_data[1]
                o.set_xdata(x_data)
            end
        end

        # Prevent modifying the limits
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

        for o in ax.findobj(mpl_text.Text)
            o.set_fontname(font)
            o.set_fontsize(fontsize)
        end

    end

    
    draw() # Only necessary when not in interactive mode
end

function figure_composer_encode_xy_data(id, x, y; fmt="ptset", yerr=nothing, xerr=nothing)
    if length(x) != length(y)
        error("X and Y series are not the same length")
    end

    # Create IO string
    io = IOBuffer()
    iob64_encode = Base64EncodePipe(io)
    write(iob64_encode, ntoh(UInt32(length(id))))
    write(iob64_encode, ascii(id)[1:length(id)])

    if fmt == "ptset"
        data_type = 0
    elseif fmt == "mset"
        data_type = 1
    elseif fmt == "series"
        data_type = 2
    elseif fmt == "mseries"
        data_type = 3
    elseif fmt == "raster1d"
        data_type = 4
    else
        error("Invalid data type specified")
    end
    write(iob64_encode, ntoh(UInt32(data_type)))

    breadth = 2
    if (xerr != nothing) && (yerr == nothing)
        breadth += 4
    elseif (yerr != nothing) && (xerr == nothing)
        breadth += 2
    elseif (yerr != nothing) && (xerr != nothing)
        breadth += 4
    end
    write(iob64_encode, ntoh(UInt32(breadth)))
    write(iob64_encode, ntoh(UInt32(length(x))))

    for i = 1:length(x)
        write(iob64_encode, ntoh(Float32(x[i])))
        write(iob64_encode, ntoh(Float32(y[i])))
        if breadth == 4
            write(iob64_encode, ntoh(Float32(yerr[i])))
            write(iob64_encode, ntoh(Float32(0)))
        elseif (breadth == 6) && (yerr == nothing)
            write(iob64_encode, ntoh(Float32(0)))
            write(iob64_encode, ntoh(Float32(3)))
            write(iob64_encode, ntoh(Float32(xerr[i])))
            write(iob64_encode, ntoh(Float32(0)))
        elseif (breadth == 6) && (yerr != nothing) && (xerr != nothing)
            write(iob64_encode, ntoh(Float32(yerr[i])))
            write(iob64_encode, ntoh(Float32(0)))
            write(iob64_encode, ntoh(Float32(xerr[i])))
            write(iob64_encode, ntoh(Float32(0)))
        end
    end

    close(iob64_encode)
    return String(take!(io))
end

function figure_composer_encode_xyz_data(id, x, y, z; fmt="xyzimg")
    if length(x) != length(y)
        error("X and Y series are not the same length")
    end

    # Create IO string
    io = IOBuffer()
    iob64_encode = Base64EncodePipe(io)
    write(iob64_encode, ntoh(UInt32(length(id))))
    write(iob64_encode, ascii(id)[1:length(id)])

    if fmt == "xyzimg"
        data_type = 5
        if length(x) != 2
            error("Expected a length of 2 [x0, x_end]")
        end
        if ndims(z) != 2
            error("Expected a z matrix with two dimensions")
        end
        write(iob64_encode, ntoh(UInt32(data_type)))
        write(iob64_encode, ntoh(Float32(x[1])))
        write(iob64_encode, ntoh(Float32(x[2])))
        write(iob64_encode, ntoh(Float32(y[1])))
        write(iob64_encode, ntoh(Float32(y[2])))
        breadth = size(z, 1)
        len = size(z, 2)

        # Reshape z so that it is a 1 dimensions array
        z = reshape(z, length(z))
    elseif fmt == "xyzset"
        data_type = 6
        write(iob64_encode, ntoh(UInt32(data_type)))
        if length(x) != length(z)
            error("Expected z to be the same length as x and y")
        end
        breadth = 3
        len = length(x)
    else
        error("Invalid data type specified")
    end

    write(iob64_encode, ntoh(UInt32(breadth)))
    write(iob64_encode, ntoh(UInt32(len)))
    
    for i = 1:length(z)
        if fmt == "xyzset"
            write(iob64_encode, ntoh(Float32(x[i])))
            write(iob64_encode, ntoh(Float32(y[i])))
            write(iob64_encode, ntoh(Float32(z[i])))
        else
            write(iob64_encode, ntoh(Float32(z[i])))
        end
    end

    close(iob64_encode)
    return String(take!(io))
end



"""
    save_figure_composer(filename, title="Figure Title")
Save the current figure (specified by matplotlib's gcf() function) as a figure
composer (*.fyp) file. Note that we issue a warning if the filename does not end
in ".fyp". This function only implements a few of the possible plotting functions
available in Figure Composer. Specifically, this function does not yet implement
any 3D or polar plotting
"""
function save_figure_composer(filename::String; title="Figure Title", fig=nothing, fig_size=nothing)
    #setdtd!(xml_doc, DTDNode("<?fyp appVersion=\"5.0.2\" schemaVersion=\"22\"?>"))
    #fyp_schema = TextNode("<?fyp appVersion=\"5.0.2\" schemaVersion=\"22\"?>")
    #link!(xml_doc, fyp_schema)

    # Define a helper function to convert colors to hex
    function color_to_hex(color)
        if isa(color, AbstractArray) && (length(color) == 3 || length(color) == 4)
            color = vec(color)
        elseif isa(color, AbstractArray) && length(color) != 0
            color = vec(ones(3) * color[1])
        end
        hex_value = mpl_colors.to_hex(color, keep_alpha=true)
        hex_value = replace(hex_value, r"^#"=>"")

        # Note the alpha channels are returned last, but stored first by FYP
        hex_value = string(hex_value[end-1:end], hex_value[1:6])
        return hex_value
    end

    function marker_to_symbol(marker)
        if marker == "s"
            return "box"
        elseif marker == "o"
            return "circle"
        elseif marker == "x"
            return "xhair"
        elseif marker == "."
            return "tee"
        elseif marker == "^"
            return "uptriangle"
        elseif marker == "v"
            return "downtriangle"
        else
            warn("Marker type '$marker' not implemented - using circle")
            return "circle"
        end
    end

    function set_axis_attributes(element, ax, limits)
        element["labelOffset"] = "0.25in" # Distance from ticks to label
        element["lineHt"] = "1.1"
        element["start"] = string(limits[1])
        element["spacer"] = "0in" # No offset from bottom of y-axis
        element["end"] = string(limits[end])
    end

    function set_axis_tick_attributes(element, limits, ticks, labels)

        element["perLogIntv"] = "1"
        if length(ticks) > 1
            if limits[2] < limits[1]
                limits = [limits[2], limits[1]]
            end
            element["start"] = string([tick for tick in ticks if tick >= limits[1]][1])
            element["end"] = string([tick for tick in ticks if tick <= limits[end]][end])
            tick_diff = mean(diff(ticks))
            element["intv"] = string(tick_diff)
        else
            element["start"] = string(limits[1])
            element["end"] = string(limits[end])
            element["intv"] = string("1")
            tick_diff = 0
        end
        element["fmt"] = "int" # Default
        if length(labels) == 0 || length(ticks) == 0
            element["fmt"] = "none" # or None for no labels
        elseif rem(tick_diff, 1) < 0.001
            # Possible values = non, int, f1, f2, f3
            element["fmt"] = "int"
        elseif rem(tick_diff, 1) < 0.01
            element["fmt"] = "f3"
        elseif rem(tick_diff, 1) < 0.1
            element["fmt"] = "f2"
        else
            element["fmt"] = "f1"
        end
    end

    function set_gridline_attributes(element, gridlines=nothing)
        # TODO: Implement grid line
        if gridlines === nothing || length(gridlines) == 0
            element["hide"] = "true"
            return
        end
        element["hide"] = "false"
        colors = gridlines[1].get_color()
        element["strokeColor"] = color_to_hex(colors)
        width = gridlines[1].get_linewidth()
        element["strokeWidth"] = string(width, "pt")
    end

    function set_line_attributes(element, line, index=1)
        element["legend"] = "true"
        element["title"] = string(line.get_label())
        element["mode"] = "polyline"
        colors = line.get_color()
        alpha = line.get_alpha()
        alpha = alpha === nothing ? 1.0 : alpha
        if isa(colors, AbstractMatrix)
            if index <= size(colors, 1)
                element["strokeColor"] = color_to_hex(vcat(colors[index, :], alpha))
            else
                element["strokeColor"] = color_to_hex(vcat(colors[1, :], alpha))
            end
        elseif isa(colors, AbstractString)
            element["strokeColor"] = color_to_hex(mpl_colors.to_rgba_array(colors, alpha=alpha))
        elseif isa(colors, AbstractVector)
            element["strokeColor"] = color_to_hex(vcat(colors, alpha))
        else
            element["strokeColor"] = color_to_hex(colors)
        end

        width = line.get_linewidth()
        if isa(width, AbstractVector) && length(width) > index
            element["strokeWidth"] = string(width[index], "pt")
        elseif isa(width, AbstractVector)
            element["strokeWidth"] = string(width[1], "pt")
        else
            element["strokeWidth"] = string(width, "pt")
        end
    end

    function set_symbol_attributes(element, line)
        if !PyCall.hasproperty(line, :get_marker)
            element["fillColor"] = color_to_hex([0, 0, 0])
            element["strokeColor"] = color_to_hex([0, 0, 0])
            element["strokeWidth"] = "0pt"
            element["size"] = "0pt"
            element["type"] = "circle"
            return
        end
        element["fillColor"] = color_to_hex(line.get_markerfacecolor())
        element["strokeColor"] = color_to_hex(line.get_markeredgecolor())
        element["strokeWidth"] = string(line.get_markeredgewidth(), "pt")
        element["size"] =  string(line.get_markersize(), "pt")
        if line.get_marker() == "None"
            element["type"] = "circle"
            element["size"] = "0pt"
        else
            element["type"] = marker_to_symbol(line.get_marker())
        end
    end

    function mpl_to_fc_fontstyle(font_weight)
        if occursin("bold", font_weight)
            return "bold"
        end
        return "plain" # Fall back to plain
    end

    function mpl_to_fc_horizontal_alignment(alignment)
        if alignment == "left"
            return "left"
        elseif alignment == "right"
            return "right"
        end
        return "center" # Default to center
    end

    function mpl_to_fc_vertical_alignment(alignment)
        if alignment == "top"
            return "top"
        elseif alignment == "bottom"
            return "bottom"
        end
        return "center"
    end

    function set_label_attributes(element, text)
        # Save a new "label" to the figure composer file. Returns the entire
        element["fillColor"] = color_to_hex(text.get_color())
        pos = text.get_position()
        element["loc"] = string("$(pos[1])u $(pos[2])u")
        element["title"] = text.get_text()
        element["fontStyle"] = mpl_to_fc_fontstyle(text.get_fontweight())
        #element["fontSize"] = string(Int(round(text.get_fontsize())))
        element["rotate"] = string(text.get_rotation())
        element["align"] = mpl_to_fc_horizontal_alignment(text.get_horizontalalignment())
        element["valign"] = mpl_to_fc_vertical_alignment(text.get_verticalalignment())
        element["id"] = length(text.get_label()) != 0 ? text.get_label() : element["title"]
        return element
     end

    if (! occursin(r"\.fyp$", filename))
        @warn "Filename does not end in *.fyp"
    end

    if fig == nothing
        fig = gcf()
    end
    axes = fig.axes

    # Create an empty XML document
    xml_doc = XMLDocument()
    # Create an attach a root node
    figure = ElementNode("figure")
    figure["strokeWidth"] = "0.01in"
    figure["strokeJoin"] = "miter"
    figure["loc"] = "0.5in 1in"
    figure["strokePat"] = "solid"
    figure["psFont"] = "Helvetica"
    figure["fontStyle"] = "plain"
    figure["title"] = title
    figure["fillColor"] = color_to_hex("k")
    figure["altFont"] = "serif"
    figure["strokeCap"] = "butt"
    figure["width"] = "6in"
    figure["fontSize"] = "8"
    figure["strokeColor"] = color_to_hex("k")
    figure["font"] = "Arial"
    figure["height"] = "4.5in"
    figure["xmlns"] = "http://www.keck.ucsf.edu/datanav/namespace/schema22"
    setroot!(xml_doc, figure)

    # Create ref element under figure
    #ref = addelement!(figure, "ref")
    ref = ElementNode("ref") # Must be added at end

    # Keep track of the total number of series (used for references (ref))
    num_series = 0

    if fig_size == nothing
        fig_size = fig.get_size_inches()
        if fig_size[1] == fig_size[2]
            fig_size[1] = 1
            fig_size[2] = 1
        else
            fig_size[1] = 2.5 # Width is 2.5 in
            fig_size[2] = 1
        end
    end
    # Loop over each axis, creating a new graph
    for (axes_index, ax) = enumerate(axes)
        is_polar = false
        is_3d = false
        if pybuiltin(:isinstance)(ax, mpl_projections.PolarAxes)
            is_polar = true
        end
        if pybuiltin(:isinstance)(ax, mpl_toolkits.mplot3d.axes3d.Axes3D)
            is_3d = true
        end
        # Create a graph subelement
        graph = nothing
        if is_polar
            graph = addelement!(figure, "pgraph")
        elseif is_3d
            graph = addelement!(figure, "graph3d")
            graph["rotate"] = string(round(haskey(ax, :azim) ? 180 + ax.azim : 180-60, digits=2))
            graph["elevate"] = string(round(haskey(ax, :elev) ? ax.elev : 30, digits=2))
            graph["backdrop"] = "openBox3D"
        else
            graph = addelement!(figure, "graph")
        end
        graph["title"] = length(ax.get_title()) == 0 ? "Graph Title" : string(ax.get_title())

        #geometry = ax.get_geometry() # Return the size as a tuple e.g, (1, 1, 1)
        rows, cols, num_1, num_2 = ax.get_subplotspec().get_geometry()
        geometry = (rows, cols, num_1 + 1)
        height = fig_size[2] / geometry[1]
        width = fig_size[1] / geometry[2]
        row = Integer(floor((geometry[3] - 1) / geometry[2])) # 0 base index
        column = (geometry[3] -1) - geometry[2] * row # 0 base index
        x_loc = (3 - fig_size[1] / 2) + column * width * 1.5
        y_loc = (2.25) - (row * height * 1.5)

        graph["width"] = string(round(width, digits=2), "in")
        graph["height"] = string(round(height, digits=2), "in")
        graph["loc"] = string(round(x_loc, digits=2), "in ", round(y_loc, digits=2), "in")

        # Create the X-axis
        xlim = ax.get_xlim()
        x_axis = addelement!(graph, is_polar ? "paxis" : "axis")
        if is_polar
            x_axis["start"] = string(xlim[1] * 180 / pi)
            x_axis["end"] = string(xlim[2] * 180 / pi)
            div = (xlim[2] - xlim[1]) * 180 / pi
            if length(ax.get_xticks()) > 0
                div = div / length(ax.get_xticks())
            end
            x_axis["pdivs"] = string("0 0 $div")
        else
            set_axis_attributes(x_axis, ax, xlim)
            if length(ax.get_xlabel()) > 0 && is_polar == false
                x_axis["title"] = ascii(ax.get_xlabel())
            end
            if is_3d
                x_axis["spacer"] = "0.05in" # Gap from y-axis
                x_axis["labelOffset"] = "0.30in" # Distance from ticks to label
            end
            # Create the X-axis ticks
            x_axis_ticks = addelement!(x_axis, "ticks")
            ticks = ax.get_xticks()
            set_axis_tick_attributes(x_axis_ticks, xlim, ticks, ax.get_xticklabels())
        end
        if ax.axison == false && is_3d == false
            x_axis["hide"] = "true"
        end

        # Create the Y-axis
        ylim = ax.get_ylim()
        y_axis = addelement!(graph, is_polar ? "paxis" : "axis")
        if is_polar
            y_axis["start"] = string(ylim[1])
            y_axis["end"] = string(ylim[2])
            div = ylim[2] - ylim[1]
            if length(ax.get_yticks()) > 0
                div = (ax.get_yticks()[end] - ax.get_yticks()[1]) / (length(ax.get_yticks()) - 1)
            end
            y_axis["pdivs"] = string("0 0 $div")
        else
            set_axis_attributes(y_axis, ax, ylim)
            # Override spacing
            y_axis["spacer"] = "0.05in" # Gap from x-axis
            y_axis["labelOffset"] = "0.30in" # Distance from ticks to label
            if length(ax.get_xlabel()) > 0
                y_axis["title"] = ascii(ax.get_ylabel())
            end
            # Create the Y-axis ticks
            y_axis_ticks = addelement!(y_axis, "ticks")
            ticks = ax.get_yticks()
            set_axis_tick_attributes(y_axis_ticks, ylim, ticks, ax.get_yticklabels())
        end
        if ax.axison == false && is_3d == false
            y_axis["hide"] = "true"
        end

        if is_3d
            graph["depth"] = string(round(max(width, height), digits=2), "in")
            zlim = ax.get_zlim()
            z_axis = addelement!(graph, is_polar ? "paxis" : "axis")
            set_axis_attributes(z_axis, ax, zlim)
            # Override spacing
            z_axis["spacer"] = "0.05in" # Gap from x-axis
            z_axis["labelOffset"] = "0.30in" # Distance from ticks to label
            if length(ax.get_xlabel()) > 0
                z_axis["title"] = ascii(ax.get_zlabel())
            end
            # Create the Y-axis ticks
            z_axis_ticks = addelement!(z_axis, "ticks")
            ticks = ax.get_zticks()
            set_axis_tick_attributes(z_axis_ticks, zlim, ticks, ax.get_zticklabels())
        end

        # Create the Z-axis
        if is_3d == false
            z_axis = addelement!(graph, "zaxis")
            z_axis["cmapnan"] = color_to_hex("w")
            z_axis["lineHt"] = "0.8"
            z_axis["gap"] = "0.2in"
            z_axis["start"] = "0"
            z_axis["end"] = "500"
            z_axis["spacer"] = "0.1in"
            z_axis["title"] = "Z AXIS"
        end

        # Create Z-axis tick attributes
        z_axis_ticks = addelement!(z_axis, "ticks")
        z_axis_ticks["intv"] = "10"

        # Create grid lines
        if is_polar == false
            x_grid_line = addelement!(graph, "gridline")
            set_gridline_attributes(x_grid_line, ax._gridOn ? ax.get_xgridlines() : nothing)

            y_grid_line = addelement!(graph, "gridline")
            set_gridline_attributes(y_grid_line, ax._gridOn ? ax.get_ygridlines() : nothing)

            if is_3d
                z_grid_line = addelement!(graph, "gridline")
                set_gridline_attributes(z_grid_line, ax._gridOn ? ax.get_zgridlines() : nothing)
            end
        end

        if is_3d == true
            back3d = addelement!(graph, "back3d")
            back3d["fillColor"] = "none"
            back3d = addelement!(graph, "back3d")
            back3d["fillColor"] = "none"
            back3d = addelement!(graph, "back3d")
            back3d["fillColor"] = "none"
        end

        # Create legend attributes
        legend = addelement!(graph, "legend")
        legend["loc"] = "100% 50%"
        legend["size"] = "0.1in"
        legend["len"] = "0.1in"
        legend["spacer"] = "0.1in"
        legend["labelOffset"] = "0.05in"
        legend["mid"] = "true"

        scatter_series = ax.collections
        for i = 1:length(scatter_series)
            if (pybuiltin(:isinstance)(scatter_series[i], mpl_collections.LineCollection) 
                || pybuiltin(:isinstance)(scatter_series[i], mpl_collections.QuadMesh) 
                || pybuiltin(:isinstance)(scatter_series[i], mpl.quiver.Quiver))
                continue # Skip any error bar series
            end
            c = scatter_series[i]
            x = c.get_offsets()[:, 1]
            if is_polar
                x = x .* 180 / pi
            end
            y = c.get_offsets()[:, 2]

            scatter = addelement!(graph, "scatter")


            scatter["title"] = c.get_label()
            scatter["type"] = "circle"
            scatter["src"] = "#src_$num_series"

            # Create a new set
            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            if c.get_array() != nothing
                scatter["size"] = string(sqrt(c.get_sizes()[1]), "pt")
                scatter["strokeWidth"] = "0pt"
                scatter["mode"] = "colorBubble"
                z = c.get_array()
                set["fmt"] = "xyzset"
                data = figure_composer_encode_xyz_data(set["id"], x, y, z, fmt=set["fmt"])
            else
                scatter["fillColor"] = color_to_hex(c.get_facecolor())
                scatter["strokeColor"] = color_to_hex(c.get_edgecolor())
                scatter["strokeWidth"] = string(c.get_linewidth()[1], "pt")
                scatter["size"] = string(sqrt(c.get_sizes()[1]), "pt")
                scatter["mode"] = "scatter"
                set["fmt"] = "ptset"
                data = figure_composer_encode_xy_data(set["id"], x, y, fmt=set["fmt"])
            end
            link!(set, TextNode(data))
            num_series += 1
        end

        containers = [get(ax."containers", PyObject, i - 1) for i in 1:length(ax.containers)] # Do not do automatic type conversion
        errorbar_series = [child for child in containers if pybuiltin(:isinstance)(child, mpl_containers.ErrorbarContainer)]
        bar_series = [child for child in containers if pybuiltin(:isinstance)(child, mpl_containers.BarContainer)]
        for i = 1:length(bar_series)
            c = bar_series[i]
            # Get the center of the rectangle for each of the children
            x = [child._x0 + child.get_width() / 2.0 for child in c]
            y = [(child.get_height() == 0 ? child._y0 : child.get_height()) for child in c]
            width = [child.get_width() for child in c]
            xerr = zeros(length(x)) .* NaN
            yerr = zeros(length(y)) .* NaN
            if c.errorbar != nothing
                error_container = c.errorbar
                error_lines = length(error_container[2]) > 0 ? error_container[2] : error_container[3]
                for error_line in error_lines
                    for (j, segment) in enumerate(error_line.get_segments())
                        if segment[2, 1] == segment[1, 1]
                            # This is a y-errorbar
                            yerr[j] = segment[2, 2] - y[j] # TODO: Assumes symmetry
                        else
                            xerr[j] = segment[2, 1] - x[j] # TODO: Assumes symmetry
                        end
                    end
                end
                # Remove our items from the list so they aren't replotted when looking for
                # line series
                if length(error_container[2]) > 0
                    deleteat!(errorbar_series, errorbar_series .== error_container)
                else
                    deleteat!(errorbar_series, [e[3][1] for e in errorbar_series] .== error_lines[1])
                end
            end
            if is_polar
                x = x .* 180 / pi
                width = width .* 180 / pi
                xerr = xerr .* 180 / pi
            end

            bar = addelement!(graph, "trace")
            bar["legend"] = "true"
            bar["title"] = string(c.get_label())
            bar["mode"] = "histogram"
            bar["baseline"] = "0"
            bar["barWidth"] = width[1]
            bar["strokeColor"] = color_to_hex(get(c, 0).get_edgecolor())
            bar["strokeWidth"] = string(get(c, 0).get_linewidth(), "pt")
            bar["fillColor"] = color_to_hex(get(c, 0).get_facecolor())
            bar["src"] = "#src_$num_series"

            symbol = addelement!(bar, "symbol")
            symbol["fillColor"] = color_to_hex(get(c, 0).get_facecolor())
            symbol["strokeColor"] = color_to_hex(get(c, 0).get_edgecolor())
            symbol["strokeWidth"] = string(get(c, 0).get_linewidth(), "pt")
            symbol["type"] = "circle"
            symbol["size"] = "0pt"

            errorbar = addelement!(bar, "ebar")
            errorbar["hide"] = "false"
            errorbar["cap"] = "bracket"
            errorbar["capSize"] = "0in" #default 0.2in
            errorbar["strokeColor"] = color_to_hex(get(c, 0).get_edgecolor())

            # Create a new set
            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "ptset"
            data = figure_composer_encode_xy_data(set["id"], x, y, xerr=xerr, yerr=yerr, fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        for i = 1:length(errorbar_series)
            c = errorbar_series[i]
            if ! pybuiltin(:isinstance)(c[1], mpl_lines.Line2D)
                continue # Skip any non - error bar series
            end
            l = c[1] # First element is actual line
            # c[2] are the cap lines (don't care about)
            # c[3] are the actual error bar lines, x and y
            x = l.get_xdata()
            y = l.get_ydata()
            xerr = [NaN for _ in 1:length(x)]
            yerr = [NaN for _ in 1:length(y)]
            for error_line in c[3]
                for (j, segment) in enumerate(error_line.get_segments())
                    if segment[2, 1] == segment[1, 1]
                        # THis is a y error bar
                        yerr[j] = segment[2, 2] - y[j] # TODO Assumes symmetry
                    else
                        xerr[j] = segment[2, 1] - x[j]
                    end
                end
            end
            if is_polar
                x = x .* 180 / pi
                xerr = xerr .* 180 / pi
            end

            line = addelement!(graph, "trace")
            set_line_attributes(line, l)
            line["src"] = "#src_$num_series"

            symbol = addelement!(line, "symbol")
            set_symbol_attributes(symbol, l)

            # Set error bar attributes
            errorbar = addelement!(line, "ebar")
            errorbar["hide"] = "false"
            errorbar["cap"] = "bracket"
            errorbar["capSize"] = "0in" #default 0.2in

            # Create a new set
            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "ptset"
            data = figure_composer_encode_xy_data(set["id"], x, y, xerr=xerr, yerr=yerr, fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        line_series = ax.lines
        for i = 1:length(line_series)
            l = line_series[i]
            # Check to see if this was part of the error bar series
            if l in [c[1] for c in ax.containers]
                continue
            end
            shaded_errorbars = PyCall.hasproperty(ax, :shaded_errorbars) ? ax.shaded_errorbars : []
            if l in [c[1] for c in shaded_errorbars]
                continue
            end
            if pybuiltin(:isinstance)(l, mpl_toolkits.mplot3d.art3d.Line3D) # Skip 3D lines
                continue
            end
            x = l.get_xdata()
            y = l.get_ydata()
            if is_polar
                x = x .* 180 / pi
            end

            line = addelement!(graph, "trace")
            set_line_attributes(line, l)
            line["src"] = "#src_$num_series"

            symbol = addelement!(line, "symbol")
            set_symbol_attributes(symbol, l)

            errorbar = addelement!(line, "ebar")
            errorbar["hide"] = "true"

            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "ptset"
            data = figure_composer_encode_xy_data(set["id"], x, y, fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        # 3D line series
        for i = 1:length(line_series)
            l = line_series[i]
            # Check to see if this was part of the error bar series
            if ! pybuiltin(:isinstance)(l, mpl_toolkits.mplot3d.art3d.Line3D) # Skip 3D lines
                continue
            end
            x, y, z = l.get_data_3d()
            scatter3d = addelement!(graph, "scatter3d")
            set_line_attributes(scatter3d, l)
            delete!(scatter3d, "mode")
            scatter3d["src"] = "#src_$num_series"
            scatter3d["stemmed"] = "false"

            symbol = addelement!(scatter3d, "symbol")
            set_symbol_attributes(symbol, l)


            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "xyzset"
            data = data = figure_composer_encode_xyz_data(set["id"], x, y, z, fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        quiver_collections = ax.collections
        for i = 1:length(quiver_collections)
            if ! pybuiltin(:isinstance)(quiver_collections[i], mpl.quiver.Quiver)
                continue
            end
            q = quiver_collections[i]
            scale = q.scale
            if scale === nothing
                lengths = sqrt.(q.U.^2 .+ q.V.^2)
                scale = max(10, sqrt(length(q.X))) * mean(lengths) * 2.5
            end
            for i = 1:length(q.X)
                line = addelement!(graph, "line")
                line["p0"] = "$(round(q.X[i], digits=4))u $(round(q.Y[i], digits=4))u"
                if q.angles == "xy"
                    line["p1"] = "$(round(q.X[i] + q.U[i] / scale, digits=4))u $(round(q.Y[i] + q.V[i] / scale, digits=4))u"
                else
                    r = sqrt(q.V[i]^2 + q.U[i]^2)
                    theta = atan(q.V[i], q.U[i])
                    u = r .* cos(theta) .* (xlim[2] - xlim[1])
                    v = r .* sin(theta) .* (ylim[2] - ylim[1])
                    line["p1"] = "$(round(q.X[i] + u / scale, digits=4))u $(round(q.Y[i] + v / scale, digits=4))u"
                end
                # Quiver is strange because things are drawn as shapes not as lines
                # so edge color is correct to set the line color in figure composer
                alpha = q.get_alpha()
                alpha = alpha === nothing ? 1.0 : alpha
                line["strokeColor"] = color_to_hex(vcat(q.get_facecolor()[1, :], alpha))
                line["fillColor"] = line["strokeColor"]
                if q.units == "width"
                    line["strokeWidth"] = "0.1pt"
                else
                    line["strokeWidth"] = "$(q.width)pt"
                end
                
                arrowhead = addelement!(line, "shape")
                arrowhead["loc"] = "100% 100%"
                arrowhead["type"] = "rightdart"
                arrowhead["strokeWidth"] = "0pt"
                arrowhead["strokeColor"] = line["strokeColor"]
                arrowhead["fillColor"] = line["strokeColor"]
                arrowhead["bkg"] = line["strokeColor"]
                arrowhead["width"] = "$(round(sqrt(q.U[i].^2 + q.V[i].^2) / scale, digits=4))in"
                arrowhead["height"] = arrowhead["width"] 
            end
        end

        line_collections = ax.collections
        for i = 1:length(line_collections)
            if ! pybuiltin(:isinstance)(line_collections[i], mpl_collections.LineCollection) 
                continue
            end

            l = line_collections[i]
            for (index, segment) = enumerate(l.get_segments())
                # segment is stored (x1, y1), (x2, y2)
                x = segment[:, 1]
                y = segment[:, 2]
                if is_polar
                    x = x .* 180 / pi
                end

                line = addelement!(graph, "trace")
                set_line_attributes(line, l, index)
                line["src"] = "#src_$num_series"

                symbol = addelement!(line, "symbol")
                set_symbol_attributes(symbol, l)

                errorbar = addelement!(line, "ebar")
                errorbar["hide"] = "true"

                set = addelement!(ref, "set")
                set["id"] = "src_$num_series"
                set["fmt"] = "ptset"
                data = figure_composer_encode_xy_data(set["id"], x, y, fmt=set["fmt"])
                link!(set, TextNode(data))
                num_series += 1
            end
        end

        shaded_errorbars = (! PyCall.hasproperty(ax, :shaded_errorbars)) ? [] : ax.shaded_errorbars
        for i = 1:length(shaded_errorbars)
            l = shaded_errorbars[i][1] # The line
            err = shaded_errorbars[i][3] # The actual errors
            is_y_err = shaded_errorbars[i][4] # True if this is a y-error otherwise false
            x = l.get_xdata()
            y = l.get_ydata()
            xerr = [NaN for _ in 1:length(x)]
            yerr = [NaN for _ in 1:length(y)]
            if is_y_err
                xerr = err
            else
                yerr = err
            end
            if is_polar
                x = x .* 180 / pi
                xerr = xerr .* 180 / pi
            end

            line = addelement!(graph, "trace")
            set_line_attributes(line, l)
            line["mode"] = "errorband"
            line["src"] = "#src_$num_series"

            symbol = addelement!(line, "symbol")
            set_symbol_attributes(symbol, l)

            if isa(err, AbstractVector)
                # The condition of symmetric errorbars
                errorbar = addelement!(line, "ebar")
                errorbar["hide"] = "false"
                errorbar["fillColor"] = color_to_hex(shaded_errorbars[i][2].get_facecolor())
                if shaded_errorbars[i][2].get_edgecolor()[4] == 0 # Not really there (alpha == 0)
                    errorbar["strokeColor"] = color_to_hex(shaded_errorbars[i][2].get_facecolor()) # Use face color
                    errorbar["strokeWidth"] = "0pt"
                else
                    errorbar["strokeColor"] = color_to_hex(shaded_errorbars[i][2].get_edgecolor())
                    errorbar["strokeWidth"] = string(shaded_errorbars[i][1].get_linewidth(), "pt")
                end
                errorbar["capSize"] = "0in"

                # Create a new set
                set = addelement!(ref, "set")
                set["id"] = "src_$num_series"
                set["fmt"] = "ptset"
                data = figure_composer_encode_xy_data(set["id"], x, y, xerr=xerr, yerr=yerr, fmt=set["fmt"])
                link!(set, TextNode(data))
                num_series += 1
            else
                # We have (potentially asymetric errorbars. Therefore, we need to save the path as a second
                # series
                errorbar = addelement!(line, "ebar")
                errorbar["hide"] = "true" # Do not show errorband on the first errorbar series
                errorbar["strokeWidth"] = "0pt"
                
                # Create a new set
                set = addelement!(ref, "set")
                set["id"] = "src_$num_series"
                set["fmt"] = "ptset"
                data = figure_composer_encode_xy_data(set["id"], x, y, fmt=set["fmt"])
                link!(set, TextNode(data))
                num_series += 1

                # Compute a new mean
                line = addelement!(graph, "trace")
                line["strokeWidth"] = "0pt" # Hidden line (corresponding to a computed "mean" of the second trace)
                line["mode"] = "errorband"
                line["src"] = "#src_$num_series"
                line["title"] = string(l.get_label(), "_asymmetric_error")
                symbol = addelement!(line, "symbol")
                symbol["size"] = "0pt"
                
                errorbar = addelement!(line, "ebar")
                errorbar["hide"] = "false"
                errorbar["fillColor"] = color_to_hex(shaded_errorbars[i][2].get_facecolor())
                if shaded_errorbars[i][2].get_edgecolor()[4] == 0 # Not really there (alpha == 0)
                    errorbar["strokeColor"] = color_to_hex(shaded_errorbars[i][2].get_facecolor()) # Use face color
                    errorbar["strokeWidth"] = "0pt"
                else
                    errorbar["strokeColor"] = color_to_hex(shaded_errorbars[i][2].get_edgecolor())
                    errorbar["strokeWidth"] = string(shaded_errorbars[i][1].get_linewidth(), "pt")
                end
                errorbar["capSize"] = "0in"
                
                # Create a new set to hold this ficticious series
                set = addelement!(ref, "set")
                set["id"] = "src_$num_series"
                set["fmt"] = "ptset"

                if is_y_err == false
                    y = (err[1, :] .+ err[2, :]) / 2.0
                    yerr = abs.(err[2, :] .- y)
                else
                    x = (err[1, :] .+ err[2, :]) / 2.0
                    xerr = abs.(err[2, :] .- x)
                end
                data = figure_composer_encode_xy_data(set["id"], x, y, xerr=xerr, yerr=yerr, fmt=set["fmt"])
                link!(set, TextNode(data))
                num_series += 1
            end
        end

        contour_series = ax.collections
        for i = 1:length(scatter_series)
            if ! pybuiltin(:isinstance)(contour_series[i], mpl_collections.QuadMesh)
                continue # Skip any error bar series
            end
            c = contour_series[i]
            x = c._coordinates[1, :, 1]
            y = c._coordinates[:, 1, 2]
            # These x-arrays are actually centered on the bins
            z = reshape(c.get_array(), length(x) - 1, length(y) - 1)
            x = (x[1], x[end])
            y = (y[1], y[end])

            # Override the z-axis
            z_axis["start"] = c.get_clim()[1]
            z_axis["end"] = c.get_clim()[2]

            contour = addelement!(graph, "contour")
            contour["mode"] = "heatMap"
            #contour["strokeColor"] = color_to_hex(c.get_edgecolor())
            contour["strokeWidth"] = string(c.get_linewidth()[1], "pt")
            contour["smooth"] = false
            contour["title"] = c.get_label()
            contour["src"] = "#src_$num_series"

            # Create a new set
            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "xyzimg"
            data = figure_composer_encode_xyz_data(set["id"], x, y, z, fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        images = ax.images
        for i = 1:length(images)
            im = images[i]
            z = im.get_array()
            x = [-0.5, size(z, 2)-0.5]
            y = [-0.5, size(z, 1)-0.5]

            # Override the z-axis
            z_axis["start"] = im.get_clim()[1]
            z_axis["end"] = im.get_clim()[2]

            contour = addelement!(graph, "contour")
            contour["mode"] = "heatMap"
            #contour["strokeColor"] = color_to_hex(c.get_edgecolor())
            contour["strokeWidth"] = string("1pt")
            contour["smooth"] = false
            contour["title"] = im.get_label()
            contour["src"] = "#src_$num_series"

            # Create a new set
            set = addelement!(ref, "set")
            set["id"] = "src_$num_series"
            set["fmt"] = "xyzimg"
            data = figure_composer_encode_xyz_data(set["id"], x, y, z', fmt=set["fmt"])
            link!(set, TextNode(data))
            num_series += 1
        end

        texts = ax.texts
        for i = 1:length(texts)
            fc_label = addelement!(graph, "label")
            set_label_attributes(fc_label, texts[i])
        end

    end # End loop over ax = axes

    # Link ref to figure
    link!(figure, ref)

    xml_string = string(xml_doc)
    # Add our FYP declaration to the output
    fp = open(filename, "w")
    lines = split(xml_string, '\n')
    write(fp, lines[1], "\n")
    write(fp, "<?fyp appVersion=\"5.0.2\" schemaVersion=\"22\"?>\n")
    for line = 2:length(lines)
        write(fp, lines[line], "\n")
    end
    close(fp)
end

"""
    dump_figure_composer(filename, [output_directory])

Given a figure composer (fyp) file, dump the contents to a series of CSV (comma
separated value) files. Each subfigure/plot within the figure composer file creates
its own directory under the input parameter `output_directory`. Each series within
that subfigure creates its own CSV file. To ensure that all items are encoded correctly,
it is necessary that the user name the subfigures and series appropriately in the original
figure composer file.
"""
function dump_figure_composer(filename::AbstractString; output_directory::AbstractString=joinpath(".", replace(filename, ".fyp" => "")), force::Bool=false)
    # Given a graph XML tag, dump it to
    function dump_graph(root, node)
        title = haskey(node, "title") ? node["title"] : "Graph"
        # Create a new directory under top-level directory
        graph_dir_name = joinpath(output_directory, title)
        if Base.Filesystem.isdir(graph_dir_name)
            index = 0
            while Base.Filesystem.isdir(graph_dir_name)
                graph_dir_name = joinpath(output_directory, string(title, "_", index))
                index = index + 1
            end

            @warn "Already found a dumped directory called $title. Graph titles are not unique. Renaming graph to $(title)_$(index)."
        end
        Base.Filesystem.mkdir(graph_dir_name)

        # Look for any available traces
        for child = eachnode(node)
            # Only look for two d type series
            if child.name == "trace" || child.name == "raster"
                dump_2d_series(root, graph_dir_name, child)
            elseif child.name == "contour" || child.name == "scatter"
                dump_3d_series(root, graph_dir_name, child)
            end
        end
    end

    function dump_2d_series(root, dir_name, node)
        title = haskey(node, "title") ? node["title"] : "trace"
        csv_file_name = string(joinpath(dir_name, title), ".csv")
        if Base.Filesystem.isfile(csv_file_name)
            index = 0
            while Base.Filesystem.isfile(csv_file_name)
                graph_dir_name = string(joinpath(dir_name, title, "_", index), ".csv")
                index = index + 1
            end
            error("Unable to dump series $title to $dir_name - file already exists. Series names are not unique. Renaming to $(title)_$(index).csv.")
        end
        fp = open(csv_file_name, "w") # Overwrite if exists
        # Attempt to look up the src
        if ! haskey(node, "src")
            error("Invalid 2D series - missing src reference")
        end
        src = replace(node["src"], "#" => "")
        # Find our source within the document
        set = find_set(root, src)
        #@assert set["fmt"] == "ptset" "Expected a ptset but got $(set["fmt"])"
        io = IOBuffer()
        iob64_decode = Base64DecodePipe(io)
        write(io, set.content)
        seekstart(io)
        id_length = ntoh(read(iob64_decode, UInt32))
        id = String(read(iob64_decode, id_length))
        data_type = ntoh(read(iob64_decode, UInt32))
        if data_type == 0
            breadth = ntoh(read(iob64_decode, UInt32))
            data_length = ntoh(read(iob64_decode, UInt32))

            # Write our header
            header = ["x", "y", "yStd", "ye", "xStd", "xe"]
            write(fp, join(header[1:breadth], ","), "\n")

            # Write the data
            for i = 1:data_length
                current_row = [ntoh(read(iob64_decode, Float32)) for index = 1:breadth]
                write(fp, join(current_row, ","), "\n")
            end
            close(fp)
        elseif data_type == 2
            x0 = ntoh(read(iob64_decode, UInt32))
            dx = ntoh(read(iob64_decode, UInt32))
            breadth = ntoh(read(iob64_decode, UInt32))
            data_length = ntoh(read(iob64_decode, UInt32))
            header = [title]
            write(fp, join(header,","), "\n")
            for i = 1:data_length
                current_row = [ntoh(read(iob64_decode, Float32))]
                write(fp, join(current_row, ","), "\n")
            end
            close(fp)
        elseif data_type == 4
            breadth = ntoh(read(iob64_decode, UInt32))
            data_length = ntoh(read(iob64_decode, UInt32))
            data_length2 = ntoh(read(iob64_decode, Float32));
            header = [title]
            write(fp, join(header,","), "\n")
            for i = 1:data_length
                current_row = [ntoh(read(iob64_decode, Float32))]
                write(fp, join(current_row, ","), "\n")
            end
            close(fp)
        else
            error("data_type $data_type not recognized for 2D objects.")
        end
    end

    function dump_3d_series(root, dir_name, node)
        title = haskey(node, "title") ? node["title"] : "trace"
        csv_file_name = string(joinpath(dir_name, title), ".csv")
        if Base.Filesystem.isfile(csv_file_name)
            index = 0
            while Base.Filesystem.isfile(csv_file_name)
                graph_dir_name = string(joinpath(dir_name, title, "_", index), ".csv")
                index = index + 1
            end
            error("Unable to dump series $title to $dir_name - file already exists. Series names are not unique. Renaming to $(title)_$(index).csv.")
        end
        fp = open(csv_file_name, "w") # Overwrite if exists
        # Attempt to look up the src
        if ! haskey(node, "src")
            error("Invalid 2D series - missing src reference")
        end
        src = replace(node["src"], "#" => "")
        # Find our source within the document
        set = find_set(root, src)
        #@assert set["fmt"] == "ptset" "Expected a ptset but got $(set["fmt"])"
        io = IOBuffer()
        iob64_decode = Base64DecodePipe(io)
        write(io, set.content)
        seekstart(io)
        id_length = ntoh(read(iob64_decode, UInt32))
        id = String(read(iob64_decode, id_length))
        data_type = ntoh(read(iob64_decode, UInt32))
        println(data_type)
        if data_type == 0
            breadth = ntoh(read(iob64_decode, UInt32))
            data_length = ntoh(read(iob64_decode, UInt32))

            # Write our header
            header = ["x", "y", "yStd", "ye", "xStd", "xe"]
            write(fp, join(header[1:breadth], ","), "\n")

            # Write the data
            for i = 1:data_length
                current_row = [ntoh(read(iob64_decode, Float32)) for index = 1:breadth]
                write(fp, join(current_row, ","), "\n")
            end
            close(fp)
        elseif data_type == 6

            breadth = ntoh(read(iob64_decode, UInt32))
            data_length = ntoh(read(iob64_decode, UInt32))

            # Write our header
            header = ["x", "y", "z", "yStd", "ye", "xStd", "xe"]
            write(fp, join(header[1:breadth], ","), "\n")

            # Write the data
            for i = 1:data_length
                current_row = [ntoh(read(iob64_decode, Float32)) for index = 1:breadth]
                write(fp, join(current_row, ","), "\n")
            end
            close(fp)
        else
            error("data_type $data_type not recognized for 3D objects.")
        end
    end

    function find_set(root, src_id)
        # Finds a set within the ref set of the XML document with a specific src
        # id
        for current_node = eachnode(root)
            if current_node.name != "ref"
                continue
            end
            for ref = eachnode(current_node)
                if ref.name != "set"
                    continue
                end
                if haskey(ref, "id") && ref["id"] == src_id
                    return ref
                end
            end
        end
        return nothing
    end


    doc = EzXML.readxml(filename)
    root = doc.root

    # Attempt to create our output directory if it does not exist
    if Base.Filesystem.isdir(output_directory) && force == false
        error("Directory exists at $output_directory. To completely ovewrite (destroys all subdirectories), pass force=true.")
    elseif Base.Filesystem.isdir(output_directory)
        Base.Filesystem.rm(output_directory, recursive=true)
    end
    Base.Filesystem.mkdir(output_directory)

    for current_node = eachnode(root)
        if current_node.name != "graph"
            continue
        end
        dump_graph(root, current_node)
        #println(current_node.name)
        #if current_node
    end
    return nothing
end

"""
    standardize_clim(figures...)

Given multiple figures with different color limits (e.g., different
surface plots), ensure that all figures use the same clim axes.
"""
function standardize_clim(figs...)
    vmin = Inf
    vmax = -Inf
    for fig in figs
        for im in fig.gca().collections
            current_min, current_max = im.get_clim()
            vmin = min(current_min, vmin)
            vmax = max(current_max, vmax)
        end
    end
    for fig in figs
        for im in fig.gca().collections
             im.set_clim(vmin, vmax)       
        end
    end
    return nothing
end

"""
    Re-export swarmplot from the seaborn library
"""
swarmplot(args...; kwargs...) = seaborn.swarmplot(args...; kwargs...)
