# Plotting Utility Functions

[![pipeline status](https://ligand.strangled.net/gitlab/herzfeldd/PrettyPlot.jl/badges/master/pipeline.svg)](https://ligand.strangled.net/gitlab/herzfeldd/PrettyPlot.jl/commits/master)
[![coverage report](https://ligand.strangled.net/gitlab/herzfeldd/PrettyPlot.jl/badges/master/coverage.svg)](https://ligand.strangled.net/gitlab/herzfeldd/PrettyPlot.jl/commits/master)
<!-- [![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://tkpapp.gitlab.io/GitlabJuliaDemo.jl/) -->

This package provides supporting functions for "pretty plotting" in the Lisberger Lab. In particular, this
package defines a set of functions that allow for the saving of Matplotlib figures as Lisberger Lab Figure
Composer FYP files. 

To facilitate the saving of shaded regions, this package also provides a convienience function
`shaded_errorbars`. While there are other ways to plot shaded bars in Matplotlib/PyPlot, this function
specifically interfaces with the interface for saving figures as Figure Composer files. Other shaded
errorbar functions may be incorrectly associated/unassociated with the appropriate series in the saved
Figure Composer file.

The general usage of the package is:
```julia
using PrettyPlot
using PyPlot

# Do the necessary plotting
# This plots a series with y-errorbars of magnitude 1.0
shaded_errorbar(1:10, rand(10), ones(10))

save_figure_composer("/path_to_fyp.fyp")
```

The resulting FYP file can then be opened in Figure Composer. 

Note that not all of the Matplotlib figure interface is implemented for saving in Figure Composer. In
fact, only a very small portion of Matplotlib/PyPlot functions are implemented. Known working 
plots include any combination of lines, bars, error bar series, shaded errorbars (as noted above), 
axis labels, series labels, axis ranges, etc. 
